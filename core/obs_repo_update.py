#!/bin/env python3
"""
created by :beihengyan
date:2020-10-26
"""
import os
import sys
import shutil
import argparse
import logging

obs_meta="obs_meta/"
obs_prj_meta="obs_meta/OBS_PRJ_meta/"

class ProjectAction(object):
    """
    create,backup,delete Project
    """
    def __init__(self, update_type, prj_name):
        """
        update_type: The type with create, backup or delete
        prj_name: The project you want operate
        """
        self.update_type = update_type.strip()
        self.prj_name = prj_name.strip()

    def _delete_prj(self, del_name):
        """
        del_name:The project you want delete
        """
        #delete_path=obs_prj_meta + del_name
        #print("delete_path:" + delete_path)
        if os.path.exists(del_name):
            shutil.rmtree(del_name)
            #print("delete %s , done" % delete_path)
            loginfo="delete " + del_name
            logging.info(loginfo)
        else:
            logging.warning("Project not exist")

    def _backup_prj(self, back_name):
        """
        back_name:The project you want backup
        """
        #backup_path=obs_meta + back_name
        #print("backup_path:" + backup_path)
        if os.path.exists(back_name):
            back_prj_name=back_name + "_bak"
            if os.path.exists(back_prj_name):
                shutil.rmtree(back_prj_name)
            shutil.copytree(back_name, back_prj_name, symlinks=True, ignore=shutil.ignore_patterns('*.pyc', 'tmp*'))
            #print("backup %s ,done " % backup_path)
            loginfo="backup " + back_name
            logging.info(loginfo)
        else:
            logging.warning("project not exist")

    def _create_prj(self, create_name):
        """
        create_name:The project you want create
        """
        #create_path=obs_prj_meta + create_name
        #print("create_path:" + create_path)
        if os.path.exists(create_name)!=1:
            os.makedirs(create_name)
            print("created project:" + create_name)
            #cd create_name dir  os.chdir(create_path)
            #osc command to create file
            #push? osc add, osc ci
            #osc meta prj -e create_name
            #
        else:
            #print(create_name+" is already,exit")
            logging.warning(create_name + " is already")

    def start(self):
        logging.basicConfig(level=logging.DEBUG,
            format="%(asctime)s %(name)s %(levelname)s %(message)s",
            datefmt='%Y-%m-%d  %H:%M:%S %a')
        if self.update_type == "create":
            print("start:create project")
            test_run._create_prj(self.prj_name)
        elif self.update_type == "backup":
            print("start:backup project")
            test_run._backup_prj(self.prj_name)
        elif self.update_type == "delete":
            print("start:delete project")
            test_run._delete_prj(self.prj_name)
        else:
            print("useless command")

if __name__ == '__main__':
    test_run=ProjectAction(sys.argv[1], sys.argv[2])
    test_run.start()
    
